/*!
 * \file
 * \brief Definition of ADC class
 */
#ifndef ESPLIB_SRC_PERIF_ADC_H_
#define ESPLIB_SRC_PERIF_ADC_H_

#include "ADCHandle.h"
#include "ADC_types.h"
#include <driver/adc.h>

/*
 * \brief Manages the ADC peripheral
 */
class ADC {
private:
  /*!
   * \brief Check if ESP ADC supports internal calibration
   */
  static void _check_efuse();
  static bool _initialized;
  static adc_bits_width_t _adc1_res;
public:
  static void init();
  static void set_ADC1_res(adc_bits_width_t res);
  static adc_bits_width_t get_ADC1_res();
  static ADCHandle get_handle(adc_t adc, adc_channel_t channel, adc_atten_t atten);
  static void power_on();
  static void power_off();

};

#endif /* ESPLIB_SRC_PERIF_ADC_H_ */
