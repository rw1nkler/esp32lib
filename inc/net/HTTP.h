#ifndef ESPLIB_SRC_NET_HTTP_H_
#define ESPLIB_SRC_NET_HTTP_H_

#include <esp_err.h>
#include <lwip/netdb.h>
#include "net/HTTPServer.h"


class HTTP {
private:
  void _init_hints(struct addrinfo &addr);
public:
  HTTP();
  ~HTTP();

  esp_err_t send_req(const char *host, const char *port, const char *req);
  HTTPServer create_server();
};

#endif /* ESPLIB_SRC_NET_HTTP_H_ */
