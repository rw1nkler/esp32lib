/*!
 * \file
 * \brief Source of ADC class
 */

#include "adc/ADC.h"

#include "sdkconfig.h"
#include <driver/adc.h>
#include <esp_adc_cal.h>
#include <esp_log.h>

/*** Private Variables ***/

static const char *ADC_TAG = "ADC";

static const char *INFO_EFUSE_SUPP = "Internal callibration supported";
static const char *INFO_EFUSE_NOT_SUPP = "Internal callibration not supported";
static const char *INFO_ADC_INITIALIZED = "ADC initialized";

/*** Static Members Initialization ***/

bool ADC::_initialized = false;
adc_bits_width_t ADC::_adc1_res = ADC_WIDTH_BIT_12;

/*** Private Methods ***/

void ADC::_check_efuse() {
  if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
    ESP_LOGI(ADC_TAG, "%s", INFO_EFUSE_SUPP);
  } else {
    ESP_LOGI(ADC_TAG, "%s", INFO_EFUSE_NOT_SUPP);
  }

  if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
    ESP_LOGI(ADC_TAG, "%s", INFO_EFUSE_SUPP);
  } else {
    ESP_LOGI(ADC_TAG, "%s", INFO_EFUSE_NOT_SUPP);
  }
}

/*** Public Methods ***/

void ADC::init() {
  adc1_config_width(ADC_WIDTH_BIT_12);
  ADC::_adc1_res = ADC_WIDTH_BIT_12;
  ADC::_initialized = true;

  ESP_LOGI(ADC_TAG, "%s", INFO_ADC_INITIALIZED);
}

void ADC::set_ADC1_res(adc_bits_width_t res) {
  if (adc1_config_width(res) == ESP_ERR_INVALID_ARG) {
  // Parameter error indicates bad usage.
    assert(0);
  }
}

adc_bits_width_t ADC::get_ADC1_res() {
 return ADC::_adc1_res;
}

ADCHandle ADC::get_handle(adc_t adc, adc_channel_t channel, adc_atten_t atten) {
  return ADCHandle(adc, channel, atten);
}

void ADC::power_on() {
  adc_power_on();
}

void ADC::power_off() {
  adc_power_off();
}

