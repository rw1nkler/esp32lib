#include "net/WiFi.h"
#include "init/NVS.h"
#include "init/TCP.h"

#include "sdkconfig.h"
#include <esp_err.h>
#include <esp_log.h>
#include <esp_wifi.h>
#include <esp_system.h>
#include <lwip/sys.h>
#include <lwip/err.h>
#include <lwip/ip4_addr.h>

#include <assert.h>
#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>

#define RETURN_ON_ERR(func) {if ((func) != ESP_OK) return ESP_FAIL;}

/* Private Variables */

static const char *WIFI_TAG = "WIFI";

static const char *ERR_EVENT_LOOP_INIT = "Unable to init event loop";
static const char *ERR_WIFI_CONFIG_INIT = "Unable to init wifi config";
static const char *ERR_SET_MODE = "Unable to set WiFi mode";
static const char *ERR_SET_CONFIG = "Unable to set WiFi config";
static const char *ERR_WIFI_START = "Unable to start WiFi";
static const char *ERR_WIFI_CONNECT = "Unable to connect WIFI";
static const char *ERR_WIFI_DISCONNECTED = "WiFi disconnected";

static const char *INFO_GOT_IP = "ESP has WiFi adress";
static const char *INFO_STA_START = "ESP STA start";
static const char *INFO_STA_DISCONNECTED = "ESP WiFi disconnected";

/* Static Members Initialization */

WiFi::state_t WiFi::_state = uninitialized;
wifi_mode_t WiFi::_mode;
wifi_config_t WiFi::_config;
EventGroupHandle_t WiFi::_evgroup;
uint8_t WiFi::_reconnect_cnt = 0;

/* Private Methods */

void WiFi::_init_event_group() {
  WiFi::_evgroup = xEventGroupCreate();
}

esp_err_t WiFi::_init_event_loop() {
  esp_err_t err_event_loop_init = esp_event_loop_init(WiFi::_event_handler, NULL);
  if (err_event_loop_init) {
    ESP_LOGE(WIFI_TAG, "%s", ERR_EVENT_LOOP_INIT);
    return err_event_loop_init;
  }
  return ESP_OK;
}

esp_err_t WiFi::_init_config() {
  wifi_init_config_t init_config = WIFI_INIT_CONFIG_DEFAULT();
  esp_err_t err_wifi_config_init = esp_wifi_init(&init_config);
  if (err_wifi_config_init) {
    ESP_LOGE(WIFI_TAG, "%s", ERR_WIFI_CONFIG_INIT);
    return err_wifi_config_init;
  }
  return ESP_OK;
}


esp_err_t WiFi::_set_mode(wifi_mode_t mode) {
  WiFi::_mode = mode;

  esp_err_t err_set_mode = esp_wifi_set_mode(mode);
  if (err_set_mode) {
    ESP_LOGE(WIFI_TAG, "%s", ERR_SET_MODE);
    return err_set_mode;
  }
  return ESP_OK;
}


esp_err_t WiFi::_set_config(wifi_config_t &conf) {
  WiFi::_config = conf;

  esp_err_t err_set_config;
  if (WiFi::_mode == WIFI_MODE_STA) {
    err_set_config = esp_wifi_set_config(ESP_IF_WIFI_STA, &WiFi::_config);
    if (err_set_config) {
      ESP_LOGE(WIFI_TAG, "%s", ERR_SET_CONFIG);
    }
  }

  return ESP_OK;
}

esp_err_t WiFi::_event_handler(void *ctx, system_event_t *event) {
  switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI(WIFI_TAG, "%s", INFO_STA_START);
      WiFi::_reconnect_cnt = 0;
      xEventGroupSetBits(WiFi::_evgroup, STARTED_BIT);
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      ESP_LOGI(WIFI_TAG, "%s"/*, IP: %s"*/, INFO_GOT_IP/*, ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip)*/);
      WiFi::_state = connected;
      xEventGroupSetBits(WiFi::_evgroup, CONNECTED_BIT);
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      WiFi::_state = disconnected;
      ESP_LOGI(WIFI_TAG, "%s", INFO_STA_DISCONNECTED);
      {
        if (WiFi::_reconnect_cnt < WiFi::MAX_RETRY_CONN) {
          esp_wifi_connect();
          xEventGroupClearBits(WiFi::_evgroup, CONNECTED_BIT);
          ++WiFi::_reconnect_cnt;
        }
        ESP_LOGE(WIFI_TAG, "%s", ERR_WIFI_DISCONNECTED);
      }
      break;
    default:
      break;
  }
  return ESP_OK;
}

/* Public Methods */

esp_err_t WiFi::init() {
  assert(WiFi::_state == uninitialized);

  RETURN_ON_ERR(NVS::init());
  TCP::init();

  WiFi::_init_event_group();
  RETURN_ON_ERR(WiFi::_init_event_loop());
  RETURN_ON_ERR(WiFi::_init_config());

  WiFi::_state = initialized;
  return ESP_OK;
}

void WiFi::def_config_sta(wifi_config_t &conf, const char* wifi_name, const char* pass) {
  assert(strlen(wifi_name) <= 32 && strlen(pass) <= 64);

  memset(&conf, '\0', sizeof(wifi_config_t));
  strcpy((char*) conf.sta.ssid, wifi_name);
  strcpy((char*) conf.sta.password, pass);
}

esp_err_t WiFi::config(wifi_mode_t mode, wifi_config_t &conf) {
  assert(WiFi::_state == initialized);

  RETURN_ON_ERR(WiFi::_set_mode(mode));
  RETURN_ON_ERR(WiFi::_set_config(conf));

  WiFi::_state = configured;
  return ESP_OK;
}

esp_err_t WiFi::connect() {
  assert(WiFi::_state == configured);
  esp_err_t err_wifi_start = esp_wifi_start();
  if (err_wifi_start) {
    ESP_LOGE(WIFI_TAG, "%s", ERR_WIFI_START);
    return err_wifi_start;
  }

  xEventGroupWaitBits(WiFi::_evgroup, WiFi::STARTED_BIT, pdFALSE, pdTRUE, pdMS_TO_TICKS(100));
  esp_err_t err_wifi_connect = esp_wifi_connect();
  if (err_wifi_connect) {
    ESP_LOGE(WIFI_TAG, "%s", ERR_WIFI_CONNECT);
    return err_wifi_connect;
  }

  return ESP_OK;
}

bool WiFi::is_connected() {
  return (WiFi::_state == connected) ? true : false;
}

EventGroupHandle_t WiFi::get_event_group() {
  return WiFi::_evgroup;
}
