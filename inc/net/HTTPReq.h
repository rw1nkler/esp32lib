#ifndef ESPLIB_SRC_NET_HTTPREQ_H_
#define ESPLIB_SRC_NET_HTTPREQ_H_

#include "sdkconfig.h"
#include <esp_err.h>

#define HTTPREQ_REQ_MAX_LEN    CONFIG_ESP32LIB_NET_HTTPREQ_REQ_MAX_LEN
#define HTTPREQ_HOST_MAX_LEN   CONFIG_ESP32LIB_NET_HTTPREQ_HOST_MAX_LEN
#define HTTPREQ_URL_MAX_LEN    CONFIG_ESP32LIB_NET_HTTPREQ_URL_MAX_LEN
#define HTTPREQ_VAR_MAX_LEN    CONFIG_ESP32LIB_NET_HTTPREQ_VAR_MAX_LEN

class HTTPReq {
private:
  char _buff[HTTPREQ_REQ_MAX_LEN];
  char _host[HTTPREQ_HOST_MAX_LEN];
  char _url[HTTPREQ_URL_MAX_LEN];
  char _var[HTTPREQ_VAR_MAX_LEN];

  uint16_t _used_var_len;

public:
  HTTPReq();
  ~HTTPReq();

  esp_err_t set_host(const char *host);
  esp_err_t set_url(const char *url);
  esp_err_t append_var(const char *name, const char *val);

  char* create_get();
  char* create_post();
};

#endif /* ESPLIB_SRC_NET_HTTPREQ_H_ */
