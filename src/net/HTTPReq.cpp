#include "sdkconfig.h"
#include <esp_log.h>
#include <string.h>
#include <assert.h>
#include "net/HTTPReq.h"

#define TMP_BUFF_SIZE 200

/* Private Variables */

static const char *HTTPREQ_TAG = "HTTPREQ";

static const char *WARN_HOST_LEN_TOO_LONG = "Host name too long";
static const char *WARN_URL_LEN_TOO_LONG = "Url too long";
static const char *WARN_VAR_TOO_LONG = "Var content too long";


/* Public Methods */

HTTPReq::HTTPReq() {
  this->_used_var_len = 0;

  memset(this->_host, '\0', HTTPREQ_HOST_MAX_LEN);
  memset(this->_url, '\0', HTTPREQ_URL_MAX_LEN);
  memset(this->_var, '\0', HTTPREQ_VAR_MAX_LEN);
}

HTTPReq::~HTTPReq() {
}

esp_err_t HTTPReq::set_host(const char *host) {
  if (strlen(host) + 1 > HTTPREQ_HOST_MAX_LEN) {
    ESP_LOGW(HTTPREQ_TAG, "%s", WARN_HOST_LEN_TOO_LONG);
    return ESP_FAIL;
  }

  memset(this->_host, '\0', HTTPREQ_HOST_MAX_LEN);
  strcpy(this->_host, host);

  return ESP_OK;
}

esp_err_t HTTPReq::set_url(const char *url) {
  if (strlen(url) + 1 > HTTPREQ_URL_MAX_LEN) {
    ESP_LOGW(HTTPREQ_TAG, "%s", WARN_URL_LEN_TOO_LONG);
    return ESP_FAIL;
  }

  memset(this->_url, '\0', HTTPREQ_URL_MAX_LEN);
  strcpy(this->_url, url);

  return ESP_OK;
}

esp_err_t HTTPReq::append_var(const char *name, const char *val) {
  if (strlen(name) + strlen(val) + 1 >= HTTPREQ_REQ_MAX_LEN - this->_used_var_len + 1) {
    ESP_LOGW(HTTPREQ_TAG, "%s", WARN_VAR_TOO_LONG);
    return ESP_FAIL;
  }

  char tmp[TMP_BUFF_SIZE];
  memset(tmp, '\0', TMP_BUFF_SIZE);
  if (strlen(this->_var) == 0) {
    sprintf(tmp, "%s=%s", name, val);
  }
  else {
    sprintf(tmp, "?%s=%s", name, val);
  }

  this->_used_var_len += strlen(tmp);
  strcat(this->_var, tmp);

  return ESP_OK;
}

char* HTTPReq::create_get() {
  assert(strcmp(this->_host, "") || strcmp(this->_url, ""));

  memset(this->_buff, '\0', HTTPREQ_REQ_MAX_LEN);
  if (strlen(this->_var) > 0) {
    sprintf(this->_buff,
          "GET %s?%s HTTP/1.0\r\n"
          "Host: %s\r\n"
          "User-Agent: esp-idf/1.0 esp32\r\n"
          "\r\n",
          this->_url, this->_var, this->_host);
  }
  else {
    sprintf(this->_buff,
            "GET %s HTTP/1.0\r\n"
            "Host: %s\r\n"
            "User-Agent: esp-idf/1.0 esp32\r\n"
            "\r\n",
            this->_url, this->_host);
  }

  return this->_buff;
}

char* HTTPReq::create_post() {
  assert(strcmp(this->_host, "") || strcmp(this->_url, "") || strcmp(this->_var, ""));

  memset(this->_buff, '\0', HTTPREQ_REQ_MAX_LEN);
  sprintf(this->_buff,
          "POST %s HTTP/1.0\r\n"
          "Host: %s\r\n"
          "User-Agent: esp-idf/1.0 esp32\r\n"
          "%s\r\n",
          this->_url, this->_host, this->_var);

  return this->_buff;
}

