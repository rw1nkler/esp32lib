/*!
 * \file
 * \brief Source of TCP class
 */

#include "init/TCP.h"

#include <esp_wifi.h>
#include <tcpip_adapter.h>

/*** Static Members Initialization ***/

bool TCP::_initialized = false;

/*!
 * \brief Ensures that TCP stack would be initialized only once
 */
void TCP::init() {
  if (!TCP::_initialized) {
    tcpip_adapter_init();
  }
}
