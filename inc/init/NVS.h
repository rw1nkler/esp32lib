/*!
 * \file
 * \brief Definition of NVS class
 */
#ifndef MAIN_NVS_H_
#define MAIN_NVS_H_

#include <esp_err.h>

class NVS {
private:
  /*!
   * \brief Remembers that NVS was initialized
   */
  static bool _initialized;
public:
  /*!
   * \brief Initializes NVS
   */
  static esp_err_t init();
};

#endif /* MAIN_NVS_H_ */
