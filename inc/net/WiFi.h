#ifndef MAIN_WIFI_H_
#define MAIN_WIFI_H_

#include <esp_err.h>
#include <esp_event_loop.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>

class WiFi {
  typedef enum {
    uninitialized,
    initialized,
    configured,
    connected,
    disconnected
  } state_t;
  static const uint8_t MAX_RETRY_CONN = 5;

private:
  static state_t _state;
  static wifi_mode_t _mode;
  static wifi_config_t _config;
  static EventGroupHandle_t _evgroup;
  static uint8_t _reconnect_cnt;

  static void _init_event_group();
  static esp_err_t _init_event_loop();
  static esp_err_t _init_config();
  static esp_err_t _set_mode(wifi_mode_t mode);
  static esp_err_t _set_config(wifi_config_t &conf);
  static esp_err_t _event_handler(void *ctx, system_event_t *event);

public:
  static const EventBits_t STARTED_BIT   = BIT0;
  static const EventBits_t CONNECTED_BIT = BIT1;

  static esp_err_t init();
  static void def_config_sta(wifi_config_t &conf, const char* wifi_name, const char* pass);
  static esp_err_t config(wifi_mode_t mode, wifi_config_t &conf);
  static esp_err_t connect();
  static bool is_connected();
  static EventGroupHandle_t get_event_group();

};

#endif /* MAIN_WIFI_H_ */
