#include "adc/ADCHandle.h"

#include "adc/ADC.h"
#include <driver/adc.h>
#include <esp_adc_cal.h>
#include <assert.h>

#define DEFAULT_VREF 1100

/*** Private Methods ***/

adc_bits_width_t ADCHandle::_get_res() {
  if (this->_adc == ADC1) {
    return ADC::get_ADC1_res();
  }
  else {
    return this->_adc2_res;
  }
}

void ADCHandle::_update_chars() {
  adc_bits_width_t res = this->_get_res();
  esp_adc_cal_characterize(ADC_UNIT_1, this->_atten, res, DEFAULT_VREF, &this->_chars);
}

void ADCHandle::_config_channel_atten() {
  if (this->_adc == ADC1) {
    adc1_config_channel_atten((adc1_channel_t) this->_channel, this->_atten);
  }
  else {
    adc2_config_channel_atten((adc2_channel_t) this->_channel, this->_atten);
  }
}

int ADCHandle::_get_raw() {
  if (this->_adc == ADC1) {
    return adc1_get_raw((adc1_channel_t) this->_channel);
  }
  else {
    int ret;
    ret = adc2_get_raw((adc2_channel_t) this->_channel, this->_get_res(), &ret);
    if (ret == ESP_ERR_TIMEOUT) {
      // ESP_ERR_TIMEOUT indicates that WiFi is running and you want to use ADC2. That's impossible!
      assert(0);
    }

    return ret;
  }
}

/*** Public Methods ***/

ADCHandle::ADCHandle(adc_t adc, adc_channel_t channel, adc_atten_t atten) {
  this->_adc = adc;
  this->_channel = channel;
  this->_atten = atten;
  this->_adc2_res = ADC_WIDTH_BIT_12;

  this->_config_channel_atten();
  this->_update_chars();
}

ADCHandle::~ADCHandle() {
}

void ADCHandle::set_adc(adc_t adc) {
  if (adc != this->_adc) {
    this->_adc = adc;
    this->_update_chars();
  }
}

void ADCHandle::set_atten(adc_atten_t atten) {
  if (atten != this->_atten) {
    this->_atten = atten;
    this->_config_channel_atten();
    this->_update_chars();
  }
}

void ADCHandle::set_channel(adc_channel_t channel) {
  if (channel != this->_channel) {
    this->_channel = channel;
    this->_config_channel_atten();
  }
}

adc_voltage_t ADCHandle::measure() {
  uint32_t raw = this->_get_raw();
  return esp_adc_cal_raw_to_voltage(raw, &this->_chars);
}

adc_voltage_t ADCHandle::measure(uint8_t samples) {
  uint32_t sum = 0;
  for (uint8_t i = 0; i < samples; ++i) {
    sum += this->_get_raw();
  }
  sum = sum / samples;

  return esp_adc_cal_raw_to_voltage(sum, &this->_chars);
}

