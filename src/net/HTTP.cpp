#include "net/HTTP.h"
#include "net/WiFi.h"

#include "sdkconfig.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <lwip/err.h>
#include <lwip/sys.h>
#include <lwip/netdb.h>
#include <lwip/dns.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_http_server.h>

#include <string.h>

/* Private Variables */

const char *HTTP_TAG = "HTTP";

const char *ERR_GETADDRINFO = "getaddrinfo() do not work";
const char *ERR_SOCK_OPEN = "Unable to open socket";
const char *ERR_CONNECT = "Unable to connect with server";
const char *ERR_SEND = "Unable to send request";

const char *INFO_STARTING_SERVER = "Http server is starting";

/* Private Methods */

void HTTP::_init_hints(struct addrinfo &addr) {
  memset((void*) &addr, '\0', sizeof(struct addrinfo));
  addr.ai_family = AF_INET;
  addr.ai_socktype = SOCK_STREAM;
}

/* Public Methods */

HTTP::HTTP() {
}

HTTP::~HTTP() {
}

esp_err_t HTTP::send_req(const char *host, const char *port, const char *req) {
  EventGroupHandle_t evgroup = WiFi::get_event_group();
  xEventGroupWaitBits(evgroup, WiFi::CONNECTED_BIT, pdFALSE, pdTRUE, portMAX_DELAY);
  if (!WiFi::is_connected()) {
    return ESP_FAIL;
  }

  struct addrinfo hints;
  this->_init_hints(hints);
  struct addrinfo *res = NULL;

  int err = getaddrinfo(host, port, &hints, &res);
  if (err != 0 || res == NULL) {
    ESP_LOGE(HTTP_TAG, "%s", ERR_GETADDRINFO);
    return ESP_FAIL;
  }

  int sock_fd = socket(res->ai_family, res->ai_socktype, 0);
  if (sock_fd < 0) {
    ESP_LOGE(HTTP_TAG, "%s", ERR_SOCK_OPEN);
    freeaddrinfo(res);
    return ESP_FAIL;
  }

  err = connect(sock_fd, res->ai_addr, res->ai_addrlen);
  if (err != 0) {
    ESP_LOGE(HTTP_TAG, "%s", ERR_CONNECT);
    freeaddrinfo(res);
    return ESP_FAIL;
  }

  freeaddrinfo(res);

  if (write(sock_fd, req, strlen(req)) < 0) {
    ESP_LOGE(HTTP_TAG, "%s", ERR_SEND);
    close(sock_fd);
  }

  int read_cnt;
  char recv_buff[64];
  do {
    bzero(recv_buff, sizeof(recv_buff));
    read_cnt = read(sock_fd, recv_buff, sizeof(recv_buff) - 1);
    for (int i = 0; i < read_cnt; ++i) {
      putchar(recv_buff[i]);
    }
  } while (read_cnt > 0);

  close(sock_fd);

  return ESP_OK;
}

