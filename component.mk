COMPONENT_ADD_INCLUDEDIRS := /inc
COMPONENT_ADD_INCLUDEDIRS += /inc/perif

COMPONENT_SRCDIRS := src/init
COMPONENT_SRCDIRS += src/net
COMPONENT_SRCDIRS += src/perif/adc
COMPONENT_SRCDIRS += src/main

# Init
$(call compile_only_if,$(CONFIG_ESP32LIB_INIT_NVS),src/init/NVS.o)
$(call compile_only_if,$(CONFIG_ESP32LIB_INIT_TCP),src/init/TCP.o)

# Net
# TODO
$(call compile_only_if,$(CONFIG_ESP32LIB_NET_WIFI),src/net/WiFi.o)
$(call compile_only_if,$(CONFIG_ESP32LIB_NET_WIFI),src/net/HTTP.o)
$(call compile_only_if,$(CONFIG_ESP32LIB_NET_WIFI),src/net/HTTPReq.o)

# Perif

## ADC
$(call compile_only_if,$(CONFIG_ESP32LIB_PERIF_ADC),src/perif/adc/ADC.o)
$(call compile_only_if,$(CONFIG_ESP32LIB_PERIF_ADC),src/perif/adc/ADCHandle.o)