#ifndef ESPLIB_SRC_NET_HTTPURI_H_
#define ESPLIB_SRC_NET_HTTPURI_H_

#include <esp_http_server.h>
#include <esp_err.h>

typedef esp_err_t (*req_handler_t)(httpd_req_t*);

class HTTPUri {
private:
  httpd_uri_t _uri_h;

  char *_uri;
  char *_user_ctx;
  bool _method_initialized;

public:
  HTTPUri();
  ~HTTPUri();

  HTTPUri& operator=(const HTTPUri& httpuri);

  void set_uri(const char *uri);
  void set_method(httpd_method_t method);
  void set_handler(esp_err_t (*handler)(httpd_req_t*));
  void set_user_ctx(const char *user_ctx);
  httpd_uri_t& get_uri_h();

  bool is_initialized() const;

};

#endif /* ESPLIB_SRC_NET_HTTPURI_H_ */
