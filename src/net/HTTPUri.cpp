#include "net/HTTPUri.h"

#include "sdkconfig.h"
#include <esp_log.h>
#include <string.h>

/* Private Variables */

const char *HTTPURI_TAG = "HTTPUri";

const char *ERR_SET_URI = "Unable to set uri";
const char *ERR_SET_USER_CTX = "Unable to set user ctx";

/* Public Methods */

HTTPUri::HTTPUri() {
  this->_uri_h.uri = nullptr;
  this->_uri_h.handler = nullptr;
  this->_uri_h.user_ctx = nullptr;

  this->_method_initialized = false;
  this->_uri = nullptr;
  this->_user_ctx = nullptr;
}

HTTPUri::~HTTPUri() {
  if (this->_uri != nullptr) {
    delete this->_uri;
  }

  if (this->_user_ctx != nullptr) {
    delete this->_user_ctx;
  }
}

HTTPUri& HTTPUri::operator=(const HTTPUri& httpuri) {

  this->_uri_h.method       = httpuri._uri_h.method;
  this->_method_initialized = httpuri._method_initialized;
  this->_uri_h.handler      = httpuri._uri_h.handler;

  int32_t uri_len = strlen(httpuri._uri);
  if (uri_len > 0) {
    this->_uri = new char[uri_len + 1];
    memcpy(this->_uri, httpuri._uri, uri_len);
    this->_uri_h.uri = this->_uri;
  }

  int32_t ctx_len = strlen(httpuri._user_ctx);
  if (ctx_len > 0) {
    this->_user_ctx = new char[ctx_len + 1];
    memcpy(this->_user_ctx, httpuri._user_ctx, ctx_len + 1);
    this->_uri_h.user_ctx = this->_user_ctx;
  }

  return *this;
}

bool HTTPUri::is_initialized() const {
  if (this->_uri_h.uri      == nullptr ||
      this->_uri_h.handler  == nullptr ||
      this->_uri_h.user_ctx == nullptr) {
    return false;
  }

  if (this->_method_initialized == false   ||
      this->_uri                == nullptr ||
      this->_user_ctx           == nullptr) {
    return false;
  }

  return true;
}

void HTTPUri::set_uri(const char *uri) {
  int32_t uri_len = strlen(uri);
  if (uri_len > 0) {
    this->_uri = new char[uri_len + 1];
    memcpy(this->_uri, uri, uri_len + 1);
    this->_uri_h.uri = this->_uri;
  }
  else {
    ESP_LOGE(HTTPURI_TAG, "%s", ERR_SET_URI);
  }
}

void HTTPUri::set_method(httpd_method_t method) {
  this->_uri_h.method = method;
  this->_method_initialized = true;
}

void HTTPUri::set_handler(esp_err_t (*handler)(httpd_req_t*)) {
  this->_uri_h.handler = handler;
}

void HTTPUri::set_user_ctx(const char *user_ctx) {
  int32_t ctx_len = strlen(user_ctx);
  if (ctx_len > 0) {
    this->_user_ctx = new char[ctx_len + 1];
    memcpy(this->_user_ctx, user_ctx, ctx_len + 1);
    this->_uri_h.user_ctx = this->_user_ctx;
  }
  else {
    ESP_LOGE(HTTPURI_TAG, "%s", ERR_SET_USER_CTX);
  }
}

httpd_uri_t& HTTPUri::get_uri_h() {
  return this->_uri_h;
}


