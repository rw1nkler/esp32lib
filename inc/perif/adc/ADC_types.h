#ifndef ESPLIB_INC_PERIF_ADC_ADC_TYPES_H_
#define ESPLIB_INC_PERIF_ADC_ADC_TYPES_H_

typedef enum {
  ADC1,
  ADC2
} adc_t;

#endif /* ESPLIB_INC_PERIF_ADC_ADC_TYPES_H_ */
