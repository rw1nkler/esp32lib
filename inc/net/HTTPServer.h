#ifndef ESPLIB_SRC_NET_HTTPSERVER_H_
#define ESPLIB_SRC_NET_HTTPSERVER_H_

#include "sdkconfig.h"
#include <esp_err.h>
#include <esp_http_server.h>

#include "net/HTTPUri.h"

class HTTPServer {
private:
  static httpd_handle_t _server;
  static HTTPUri *_uri_h_tab;
  static char _req_buff[CONFIG_ESP32LIB_NET_HTTPSERVER_REQ_MAX_LEN];


public:
  static esp_err_t init();
  static esp_err_t deinit();
  static esp_err_t std_get_h(httpd_req_t *req);
  static esp_err_t reg_uri_handler(HTTPUri &uri_h);

#ifdef CONFIG_ESP32LIB_NET_HTTPSERVER_TEST
public:
  static esp_err_t reg_hello_h();
private:
  static httpd_uri_t _hello_uri_h;
#endif
};

#endif /* ESPLIB_SRC_NET_HTTPSERVER_H_ */
