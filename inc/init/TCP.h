/*!
 * \file
 * \brief Definition of TCP class
 */

#ifndef MAIN_TCP_H_
#define MAIN_TCP_H_

class TCP {
private:
  /*!
   * \brief Remembers that TCP stack was initialized
   */
  static bool _initialized;
public:
  /*!
   * \brief Initializes TCP stack
   */
  static void init();
};

#endif /* MAIN_TCP_H_ */
