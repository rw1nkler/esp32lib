#ifndef ESPLIB_SRC_DATA_H_
#define ESPLIB_SRC_DATA_H_

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include "sdkconfig.h"
#include <esp_log.h>
#include <stdio.h>

extern const char *DATA_TAG;
extern const char *DATA_ERR_CREATE_MUTEX;
extern const char *DATA_ERR_TAKE_MUTEX;

template <typename T>
class Data {
private:
  SemaphoreHandle_t _mutex;
  T _val;
public:
  Data();
  ~Data();

  void set_value(const T& val);
  const T& get_value() const;
};

template <typename T>
Data<T>::Data() {
  this->_mutex = xSemaphoreCreateMutex();
  if (this->_mutex == nullptr) {
    ESP_LOGE(DATA_TAG, "%s", DATA_ERR_CREATE_MUTEX);
  }
}

template <typename T>
Data<T>::~Data() {
  vSemaphoreDelete(this->_mutex);
}

template <typename T>
void Data<T>::set_value(const T& val) {
  if (xSemaphoreTake(this->_mutex, pdMS_TO_TICKS(100)) == pdTRUE){
    this->_val = val;
    xSemaphoreGive(this->_mutex);
  }
  else {
    ESP_LOGE(DATA_TAG, "%s", DATA_ERR_TAKE_MUTEX);
  }
}

template <typename T>
const T& Data<T>::get_value() const {
  if (xSemaphoreTake(this->_mutex, pdMS_TO_TICKS(100)) == pdTRUE) {
    return this->_val;
    xSemaphoreGive(this->_mutex);
  }
  else {
    ESP_LOGE(DATA_TAG, "%s", DATA_ERR_TAKE_MUTEX);
  }
}


#endif /* ESPLIB_SRC_DATA_H_ */
