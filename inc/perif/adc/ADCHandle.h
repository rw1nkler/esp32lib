#ifndef ESPLIB_SRC_PERIF_ADCHANDLE_H_
#define ESPLIB_SRC_PERIF_ADCHANDLE_H_

#include "ADC_types.h"
#include <driver/adc.h>
#include <esp_adc_cal.h>

typedef uint32_t adc_voltage_t;

class ADCHandle {
private:
  adc_t _adc;
  adc_channel_t _channel;
  adc_atten_t _atten;
  adc_bits_width_t _adc2_res;
  esp_adc_cal_characteristics_t _chars;

  adc_bits_width_t _get_res();
  void _update_chars();
  void _config_channel_atten();
  int _get_raw();

public:
  ADCHandle(adc_t adc, adc_channel_t channel, adc_atten_t atten);
  ~ADCHandle();

  void set_adc(adc_t adc);
  void set_atten(adc_atten_t atten);
  void set_channel(adc_channel_t channel);

  adc_voltage_t measure();
  adc_voltage_t measure(uint8_t samples);
};

#endif /* ESPLIB_SRC_PERIF_ADCHANDLE_H_ */
