#include "net/HTTPServer.h"

#include "net/WiFi.h"

#include "sdkconfig.h"
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <esp_log.h>
#include <esp_err.h>
#include <string.h>

#define REQ_MAX_LEN CONFIG_ESP32LIB_NET_HTTPSERVER_REQ_MAX_LEN

/* Private Variables */

static const char *HTTPSERVER_TAG = "HTTPServer";

static const char *ERR_WIFI_UNCONNECTED = "WiFi is still unconnected";
static const char *ERR_SERVER_START = "Unable to start http server";
static const char *ERR_SERVER_STOP = "Unable to stop http server";
static const char *ERR_REGISTER_HANDLER = "Unable to register uri handler";
static const char *ERR_REQ_BUFF_TOO_SHORT = "Http req buffer is too short";

static const char *INFO_SERVER_START = "Http server is starting";
static const char *INFO_HEADER_FOUND = "Http header found";

/* Static Member Initialization */

httpd_handle_t HTTPServer::_server;
char HTTPServer::_req_buff[REQ_MAX_LEN];

#ifdef CONFIG_ESP32LIB_NET_HTTPSERVER_TEST
httpd_uri_t HTTPServer::_hello_uri_h;
#endif

/* Public Methods */

esp_err_t HTTPServer::init() {
  EventGroupHandle_t evgroup = WiFi::get_event_group();
  xEventGroupWaitBits(evgroup, WiFi::CONNECTED_BIT, pdFALSE, pdTRUE, portMAX_DELAY);
  if (!WiFi::is_connected()) {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_WIFI_UNCONNECTED);
    return ESP_FAIL;
  }

  httpd_config_t httpd_config = HTTPD_DEFAULT_CONFIG();
  ESP_LOGI(HTTPSERVER_TAG, "%s, on port: %d", INFO_SERVER_START, httpd_config.server_port);

  if (httpd_start(&HTTPServer::_server, &httpd_config) != ESP_OK) {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_SERVER_START);
    return ESP_FAIL;
  }

#ifdef CONFIG_ESP32LIB_NET_HTTPSERVER_TEST
  HTTPServer::reg_hello_h();
#endif

  return ESP_OK;
}

esp_err_t HTTPServer::deinit() {
  if (httpd_stop(&HTTPServer::_server) != ESP_OK) {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_SERVER_STOP);
    return ESP_FAIL;
  }

  return ESP_OK;
}

esp_err_t HTTPServer::std_get_h(httpd_req_t *req) {
  uint32_t header_len = httpd_req_get_hdr_value_len(req, "Host") + 1;

  if (header_len <= REQ_MAX_LEN) {
    memset(HTTPServer::_req_buff, '\0', REQ_MAX_LEN);
    if (httpd_req_get_hdr_value_str(req, "Host", HTTPServer::_req_buff, REQ_MAX_LEN) == ESP_OK) {
      ESP_LOGI(HTTPSERVER_TAG, "%s, Host: %s", INFO_HEADER_FOUND, HTTPServer::_req_buff);
    }
  }
  else {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_REQ_BUFF_TOO_SHORT);
  }

  header_len = httpd_req_get_hdr_value_len(req, "User-Agent") + 1;
  if (header_len <= REQ_MAX_LEN) {
    memset(HTTPServer::_req_buff, '\0', REQ_MAX_LEN);
    if (httpd_req_get_hdr_value_str(req, "User-Agent", HTTPServer::_req_buff, REQ_MAX_LEN) == ESP_OK) {
      ESP_LOGI(HTTPSERVER_TAG, "%s, User-Agent: %s", INFO_HEADER_FOUND, HTTPServer::_req_buff);
    }
  }
  else {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_REQ_BUFF_TOO_SHORT);
  }

  httpd_resp_send(req, (char*) req->user_ctx, strlen((char*) req->user_ctx));

  return ESP_OK;
}

esp_err_t HTTPServer::reg_uri_handler(HTTPUri& uri_h) {
  if (httpd_register_uri_handler(HTTPServer::_server, &uri_h.get_uri_h()) != ESP_OK) {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_REGISTER_HANDLER);
    return ESP_FAIL;
  }

  return ESP_OK;
}

#ifdef CONFIG_ESP32LIB_NET_HTTPSERVER_TEST

esp_err_t HTTPServer::reg_hello_h() {
  HTTPServer::_hello_uri_h.uri       = "/hello";
  HTTPServer::_hello_uri_h.method    = HTTP_GET;
  HTTPServer::_hello_uri_h.handler   = HTTPServer::std_get_h;
  HTTPServer::_hello_uri_h.user_ctx  = const_cast<char*> ("Hello World!");

  if (httpd_register_uri_handler(HTTPServer::_server, &HTTPServer::_hello_uri_h) != ESP_OK) {
    ESP_LOGE(HTTPSERVER_TAG, "%s", ERR_REGISTER_HANDLER);
    return ESP_FAIL;
  }

  return ESP_OK;
}

#endif

