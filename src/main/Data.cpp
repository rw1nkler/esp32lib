#include "main/Data.h"

/* It is a template. Implementation in header file */

const char *DATA_TAG = "Data";

const char *DATA_ERR_CREATE_MUTEX = "Unable to create mutex";
const char *DATA_ERR_TAKE_MUTEX = "Unable to take mutex";
