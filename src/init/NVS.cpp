/*!
 * \file
 * \brief Source of NVS initializer
 *
 * Class ensures that NVS will be initialized only once.
 */

#include "init/NVS.h"

#include "sdkconfig.h"
#include <esp_err.h>
#include <esp_log.h>
#include <nvs_flash.h>

/*** Private Variables ***/

static const char *NVS_TAG = "NVS";

static const char *ERR_NVS_INIT = "Unable to init NVS";
static const char *ERR_NVS_ERASE = "Unable to erase NVS";

/*** Private Members Initialization ***/

bool NVS::_initialized = false;

/*** Public Methods ***/

/*!
 * Ensures that NVS would be initialized only once
 *
 * \return ESP error code
 */
esp_err_t NVS::init() {
  if (!NVS::_initialized) {
    esp_err_t err_nvs_init = nvs_flash_init();
    if (err_nvs_init) {
      ESP_LOGW(NVS_TAG, "%s", ERR_NVS_INIT);
    }

    if (err_nvs_init == ESP_ERR_NVS_NO_FREE_PAGES || err_nvs_init == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      esp_err_t err_nvs_erase = nvs_flash_erase();
      if (err_nvs_erase) {
        ESP_LOGE(NVS_TAG, "%s", ERR_NVS_ERASE);
        return err_nvs_erase;
      }

      err_nvs_init = nvs_flash_init();
      if (err_nvs_init) {
        ESP_LOGE(NVS_TAG, "%s", ERR_NVS_INIT);
        return err_nvs_init;
      }
    }

    NVS::_initialized = true;
    return ESP_OK;
  }

  return ESP_OK;
}
